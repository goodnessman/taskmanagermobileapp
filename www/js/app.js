'use strict';

/**
 * angular.module is a global place for creating, registering and retrieving Angular modules
 * 'app' is the name of this angular module example (also set in a <body> attribute in index.html)
 * the 2nd parameter is an array of 'requires'
 */

var app = angular.module('app', ['ionic', 'LocalStorageModule', 'ui.calendar', 'ui.bootstrap', 'ui.router', 'rx', 'googleApi', 'ngCordova'], function ($httpProvider) {
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */
    var param = function param(obj) {
        var query = '',
            name,
            value,
            fullSubName,
            subName,
            subValue,
            innerObj,
            i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            } else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            } else if (value !== undefined && value !== null) query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});

app.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
});

app.constant('config', {
    localStorage: {
        events: 'events',
        tasks: 'tasks',
        years: 'years',
        months: 'months',
        days: 'days'
    },
    api: {
        tasks: 'tasks.json',
        allUsers: 'https://hydro-goose-96090.herokuapp.com/api/users',
        sendTasks: 'https://hydro-goose-96090.herokuapp.com/api/user',
        getTasks: 'https://hydro-goose-96090.herokuapp.com/api/users/'
    }
});

app.config(function (localStorageServiceProvider, $stateProvider, $urlRouterProvider, googleLoginProvider) {
    googleLoginProvider.configure({
        clientId: '239511214798.apps.googleusercontent.com',
        scopes: ["https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/calendar", "https://www.googleapis.com/auth/plus.login"]
    });

    localStorageServiceProvider.setPrefix('app');

    $urlRouterProvider.otherwise('calendar');

    $stateProvider.state('calendar', {
        url: '/calendar',
        templateUrl: 'js/calendar/calendar-template.html'
    }).state('graph', {
        url: '/graph/:id',
        templateUrl: 'js/graph/graph-template.html'
    }).state('tasks', {
        url: '/tasks',
        templateUrl: 'js/tasks/tasks-template.html'
    }).state('archive', {
        url: '/archive',
        templateUrl: 'js/archive/archive-template.html'
    }).state('data', {
        url: '/data',
        templateUrl: 'js/data/data-template.html'
    }).state('googleCalendar', {
        url: '/googleCalendar',
        templateUrl: 'js/googleCalendar/googleCalendar-template.html'
    }).state('events', {
        url: '/events',
        templateUrl: 'js/eventsComp/events-template.html'
    });
    ;
});