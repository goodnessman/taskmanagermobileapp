'use strict';

(function () {
    'use strict';

    angular.module('app').component('dataloadComponent', {
        templateUrl: 'js/data/data.html',
        controller: dataController
    });

    function dataController(config, StorageService, MainService, DataComponentRestService) {
        var ctrl = this,
            uuid = MainService.getProperty('uuid'),
            componentConfig = {
            serverNotEnabled: 'Connection to server failed!',
            errorReadData: 'Error read data from server!'
        };

        ctrl.sendData = sendData;
        ctrl.getData = getData;
        ctrl.responseStatus = '';

        checkServerConnection();

        // Check server connection
        function checkServerConnection() {
            DataComponentRestService.serverRequest('GET', config.api.allUsers).then(function (data) {
                ctrl.responseStatus = data.data.message;
            }, function (reason) {
                ctrl.responseStatus = componentConfig.serverNotEnabled;
                console.log('Connection error', reason);
            });
        }

        // Save data to bd
        function sendData() {
            var events = StorageService.getEvents();

            DataComponentRestService.serverRequest('POST', config.api.sendTasks, { "uuid": uuid, "tasks": JSON.stringify(events) }).then(function (data) {
                ctrl.responseStatus = data.data.message;
            }, function (reason) {
                ctrl.responseStatus = componentConfig.serverNotEnabled;
                console.log('Connection error', reason);
            });
        }

        // Load data from bd
        function getData() {
            DataComponentRestService.serverRequest('GET', config.api.getTasks + uuid).then(function (data) {
                var events = null;

                if (data.data.data.tasks) {
                    try {
                        events = JSON.parse(data.data.data.tasks);
                    } catch (e) {
                        ctrl.responseStatus = componentConfig.errorReadData;
                        return;
                    }
                    StorageService.setEvents(events);
                    ctrl.responseStatus = data.data.message;
                } else {
                    ctrl.responseStatus = componentConfig.errorReadData;
                }
            }, function (reason) {
                ctrl.responseStatus = componentConfig.serverNotEnabled;
                console.log('Connection error', reason);
            });
        }
    }
})();