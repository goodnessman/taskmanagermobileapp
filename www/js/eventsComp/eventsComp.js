'use strict';

(function () {
    'use strict';

    angular.module('app').component('eventsComp', {
        templateUrl: 'js/eventsComp/eventsComp.html',
        controller: eventsCompController
    });

    function eventsCompController($log, $state, $scope, $ionicModal, CalendarService, MainService, StorageService) {
        var ctrl = this;
        var currentEvent = {};
        var date = new Date();

        ctrl.tasks = [];
        ctrl.categories = [];
        ctrl.month = CalendarService.getMonth();
        ctrl.days = CalendarService.getDays();
        ctrl.activeDay = null;
        // poppup
        ctrl.poppupStatus = MainService.getProperty('popupState');
        ctrl.createEventError = {};
        ctrl.activeEvent = MainService.getActiveEvent();
        ctrl.dayMenuTitle = '';
        ctrl.dayEvents = [];
        // modals
        ctrl.newTaskModal = null;
        // events
        ctrl.events = StorageService.getEvents();
        ctrl.event = MainService.getProperty('activeEvent');
        ctrl.event.startTime = date;
        ctrl.eventSources = [];

        // functions
        ctrl.prevMonth = prevMonth;
        ctrl.nextMonth = nextMonth;
        ctrl.dayClick = dayClick;
        // popup
        ctrl.closeTaskModal = closeTaskModal;
        ctrl.createEventButton = createEventButton;
        ctrl.eventFormAction = eventFormAction;
        ctrl.openDayEvent = openDayEvent;
        ctrl.removeEventButton = removeEventButton;
        ctrl.removeEvent = removeEvent;
        ctrl.editEventButton = editEventButton;
        ctrl.editEvent = editEvent;
        ctrl.postponeButton = postponeButton;
        ctrl.addEvToArchive = addEvToArchive;
        ctrl.createSubtaskButton = createSubtaskButton;
        ctrl.configureNewEvent = configureNewEvent;

        //configure the ionic modal before use
        $ionicModal.fromTemplateUrl('new-task-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            ctrl.newTaskModal = modal;
        });

        $ionicModal.fromTemplateUrl('remove-event-question.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            ctrl.removeEventQuestion = modal;
        });

        // Add event to archive
        function addEvToArchive() {
            CalendarService.setEventToArchive(ctrl.activeEvent);
            ctrl.activeEvent = null;
            $scope.$broadcast('eventChanged');
            ctrl.newTaskModal.hide();
        }

        // Set ald events for today
        function setOldEventsForToday() {
            CalendarService.setOldEventsForToday();
            $scope.$broadcast('eventChanged');
        }
        setOldEventsForToday();

        // Update events if has new feature
        function updateEventFeature() {
            CalendarService.updateEventFeature();
        }
        // updateEventFeature();

        // Add point to calendar
        function addPointToCalendar() {
            var appState = MainService.getAppState(),
                activeEventID = MainService.getActiveEventID(),
                event = MainService.findEventByID(activeEventID);

            if (ctrl.activeDay && event && appState === 'point-to-calendar') {
                event.date = ctrl.activeDay.date;
                CalendarService.editEvent(event);
                ctrl.events = StorageService.getEvents();
                $scope.$broadcast('eventChanged');
                MainService.setAppState('free');
                currentEvent = {};
                ctrl.poppupStatus = 'dayMenu';
            } else {
                $log.error('Error adding point to calendar');
            }
        }

        // Postpone event on new date
        function postponeEvent() {
            var appState = MainService.getAppState(),
                activeEvent = MainService.getActiveEvent();

            if (ctrl.activeDay && activeEvent && appState === 'choose-day') {
                activeEvent.date.day = ctrl.activeDay.date.day;
                activeEvent.date.month = ctrl.activeDay.date.month;
                activeEvent.date.year = ctrl.activeDay.date.year;
                activeEvent.date.fullDate = ctrl.activeDay.date.fullDate;
                if (activeEvent.hasOwnProperty('type') && activeEvent.type && activeEvent.type === 'archive') {
                    activeEvent.date.startDate = ctrl.activeDay.date.fullDate;
                }
                CalendarService.editEvent(activeEvent);
                MainService.setActiveEvent();
                ctrl.activeDay = {};
                MainService.setAppState('free');
                ctrl.poppupStatus = 'dayMenu';
                $scope.$broadcast('eventChanged');
            }
        };

        // Chose date for event
        function postponeButton() {
            MainService.setAppState('choose-day');
            ctrl.activeEvent = {};
            ctrl.event = {};
            ctrl.poppupStatus = 'dayMenu';
            ctrl.newTaskModal.hide();
        };

        // Edit current event
        function editEvent() {
            CalendarService.editEvent(ctrl.event);
            ctrl.closeTaskModal();
        };

        // Open poppup edit event
        function editEventButton() {
            ctrl.event = MainService.getActiveEvent();
            ctrl.poppupStatus = 'editEvent';
        };

        // Remove event
        function removeEvent(status) {
            CalendarService.deleteEvent(ctrl.activeEvent, status);
            ctrl.closeTaskModal();
        };

        // Open poppup remove event
        function removeEventButton() {
            ctrl.removeEventQuestion.show();
        };

        // Open event from day menu
        function openDayEvent(ev) {
            ctrl.activeEvent = ev;
            MainService.setActiveEvent(ev);
            ctrl.event = ev;
            ctrl.poppupStatus = 'eventShow';
        };

        // start adding or edit event
        function eventFormAction() {
            if (ctrl.poppupStatus === 'addEvent') {
                createEvent();
            } else if (ctrl.poppupStatus === 'addSubtask') {
                createEvent('addSubtask');
            } else if (ctrl.poppupStatus === 'editEvent') {
                ctrl.editEvent(ctrl.activeEvent);
            } else {
                ctrl.editEvent(ctrl.activeEvent);
            }
        };

        // Create new event
        function createEvent(status) {
            var result = {},
                currentEvent,
                dayEvents;

            if (ctrl.event.title && ctrl.event.title.length > 0) {
                ctrl.createEventError.error = false;
                ctrl.createEventError.message = '';
                status === 'addSubtask' ? ctrl.event.type = 'subtask' : ctrl.event.type = 'default';
                result = CalendarService.createEvent(ctrl.event, ctrl.activeDay, ctrl.activeEvent);
                if (status === 'addSubtask') {
                    ctrl.event.type = 'subtask';
                    CalendarService.addSubtask(ctrl.activeEvent, result.data);
                }
                if (!result.error) {
                    currentEvent = result.data;
                    ctrl.activeEvent = {};
                    currentEvent = {};
                    ctrl.event = {};
                    ctrl.poppupStatus = 'dayMenu';
                    dayEvents = CalendarService.getDayEvents(ctrl.activeDay, 'dayMenu');
                    ctrl.dayEvents = CalendarService.sortDayEvents(dayEvents);
                    $scope.$broadcast('eventChanged');
                } else {
                    ctrl.createEventError.error = true;
                    ctrl.createEventError.message = result.message;
                    return false;
                }
            } else {
                ctrl.createEventError.error = true;
                ctrl.createEventError.message = 'Please write title for event';
                $log.warn("Title not filled");
                return false;
            }
        };

        // Set default options for new event
        function configureNewEvent() {
            if (ctrl.poppupStatus != 'editEvent') {
                ctrl.event.rating = 3;
                ctrl.event.repeat = {};
                ctrl.event.repeat.repeatDays = {};
                ctrl.event.repeat.repeatDays.mo = true;
                ctrl.event.repeat.repeatDays.tu = true;
                ctrl.event.repeat.repeatDays.we = true;
                ctrl.event.repeat.repeatDays.th = true;
                ctrl.event.repeat.repeatDays.fr = true;
                ctrl.event.repeat.repeatDays.sa = true;
                ctrl.event.repeat.repeatDays.su = true;
                if (ctrl.poppupStatus === 'addEvent' || ctrl.poppupStatus === 'addSubtask') {
                    ctrl.event.doneStatus = true;
                }
            }
        }

        // Open poppup adding event
        function createEventButton() {
            ctrl.poppupStatus = 'addEvent';
        };

        // Open poppup create subtask
        function createSubtaskButton() {
            ctrl.event = {};
            ctrl.event.rating = 3;
            ctrl.poppupStatus = 'addSubtask';
        }

        // Close poppup
        function closeTaskModal() {
            var routerPrevState = MainService.getProperty('routerPrevState');

            if (ctrl.poppupStatus === 'dayMenu') {
                ctrl.newTaskModal.hide();
            } else if (routerPrevState === 'archive') {
                $state.go('archive');
            }
        };

        // Open day menu
        function dayClick(date) {
            var appStatus = MainService.getAppState();

            ctrl.activeDay = date;
            if (appStatus === 'free') {
                openDayMenu(date);
                return true;
            } else if (appStatus === 'choose-day') {
                postponeEvent();
            } else if (appStatus === 'point-to-calendar') {
                addPointToCalendar();
            }
        };

        // Open day menu
        function openDayMenu() {
            var dayEvents = CalendarService.getDayEvents(ctrl.activeDay, 'dayMenu');

            ctrl.createEventError.error = false;
            ctrl.dayEvents = CalendarService.sortDayEvents(dayEvents);
            ctrl.dayMenuTitle = moment(ctrl.activeDay.date.fullDate).format('DD') + '-' + moment(ctrl.activeDay.date.fullDate).format('MM') + '-' + moment(ctrl.activeDay.date.fullDate).format('YYYY') + ' ' + moment(ctrl.activeDay.date.fullDate).format('dddd');
            ctrl.newTaskModal.show();
        };

        function prevMonth() {
            CalendarService.prev();
            ctrl.month = CalendarService.getMonth();
            ctrl.days = CalendarService.getDays();
        }

        function nextMonth() {
            CalendarService.next();
            ctrl.month = CalendarService.getMonth();
            ctrl.days = CalendarService.getDays();
        }
    }
})();