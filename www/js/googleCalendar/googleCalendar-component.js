'use strict';

(function () {
  'use strict';

  angular.module('app').component('googlecalendarComponent', {
    templateUrl: 'js/googleCalendar/googleCalendar.html',
    controller: googleCalendarController
  });

  function googleCalendarController($scope, $http, StorageService, googleLogin, googleCalendar, googlePlus) {
    var ctrl = this;

    $scope.login = function () {
      googleLogin.login();
    };

    $scope.$on("googlePlus:loaded", function () {
      googlePlus.getCurrentUser().then(function (user) {
        $scope.currentUser = user;
      });
    });
    $scope.currentUser = googleLogin.currentUser;

    $scope.loadEvents = function () {
      this.calendarItems = googleCalendar.listEvents({ calendarId: this.selectedCalendar.id });
    };

    $scope.loadCalendars = function () {
      $scope.calendars = googleCalendar.listCalendars();
    };
  }
})();