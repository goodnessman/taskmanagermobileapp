'use strict';

(function () {
    'use strict';

    angular.module('app').factory('TasksService', function ($log, StorageService, MainService, CalendarService) {

        var Service = {

            // Remove task
            removeTask: function removeTask(task) {
                var tasks = StorageService.getTasks();

                angular.forEach(tasks, function (tk, index) {
                    if (task.id === tk.id) {
                        angular.forEach(tk.points, function (point) {
                            CalendarService.deleteEvent(point.event);
                        });
                        tasks.splice(index, 1);
                    }
                });
                StorageService.setTasks(tasks);
            },

            // Edit task
            editTask: function editTask(tk) {
                var tasks = StorageService.getTasks();

                angular.forEach(tasks, function (task, index) {
                    if (task.id === tk.id) {
                        task.title = tk.title;
                        task = MainService.createShirtTitle(task, 'long');
                    }
                });
                StorageService.setTasks(tasks);
            }
        };

        return Service;
    });
})();