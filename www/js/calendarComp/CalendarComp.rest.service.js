'use strict';

(function () {
    'use strict';

    angular.module('app').factory('CalendarCompRestService', function ($http, $q, config) {
        var Service = {

            getTasks: function getTasks() {
                var deferred = $q.defer();

                $http.get(config.api.tasks).success(function (response, status, headers, config) {

                    deferred.resolve(response, status, headers, config);
                }).error(function (response, status, headers, config) {

                    deferred.reject(response, status, headers, config);
                });

                return deferred.promise;
            }
        };

        return Service;
    });
})();