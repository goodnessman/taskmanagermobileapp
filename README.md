
Version: beta-version-0.24.2

Application versions: 1.2.3
	1 - realise versions;
	2 - pc commits;
	3 - changes for working on android device;
	
Run project: ionic serve --lab

Build project: ionic build android

APK route: c:/WWW/mobile app/ionic/TaskManagerMobileApp/platforms/android/build/outputs/apk/

Remote device fix: adb devices

Samsung J5 uuid: 948fdcf2daa691c1

SDK: c:\Users\Nikita\AppData\Local\Android\sdk\

API:
    post: http://goodnessman.hol.es/setData.php
    get: http://goodnessman.hol.es/getData.php?username=948fdcf2daa6

App state:
    edit-event;
    archive-view: archive main page;
    remove-event;

Events:
    eventChanged;
    editEvent;

Popup status:
    editEvent;
    removeEvent;
    