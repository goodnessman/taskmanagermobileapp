(function() {
	'use strict';

	angular
	.module('app')
	.component('chainTasksComp', {
		templateUrl: 'js/tasks/tasks.html',
		controller: tasksController
	});

	function tasksController(
		$scope, 
		$ionicModal, 
		$log, 
		$element, 
		$attrs, 
		$state,
		StorageService,
		localStorageService, 
		CalendarService, 
		MainService,
		TasksService) {
		var ctrl = this,
			tasksData = 'tasks';
			
		// form
		ctrl.task = {};
		ctrl.task.title = '';

		ctrl.tasks = StorageService.getTasks();
		ctrl.questionModalTitle = '';
		ctrl.currentTask = null;
		ctrl.actionStatus = 'addTask';
		ctrl.actionTaskError = '';

		// functions
		ctrl.removeTask = removeTask;
		ctrl.removeTaskButton = removeTaskButton;
		ctrl.editTask = editTask;

		//configure the ionic modal before use
		$ionicModal.fromTemplateUrl('create-task.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then((modal) => {
			ctrl.createTaskModal = modal;
		});

		$ionicModal.fromTemplateUrl('action-question.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then((modal) => {
			ctrl.actionQuestionModal = modal;
		});

		// Change action status
		function editTask(task) {
			ctrl.actionStatus = 'editTask';
			ctrl.currentTask = task;
			ctrl.task = ctrl.currentTask;
			ctrl.createTaskModal.show();
		};

		// Open poppup remove task question
		function removeTaskButton(task) {
			ctrl.currentTask = task;
			ctrl.actionQuestionModal.show();
		};

		// Remove task
		function removeTask() {
			TasksService.removeTask(ctrl.currentTask);
			ctrl.tasks = StorageService.getTasks();
			ctrl.actionQuestionModal.hide();
		};

		// Open poppup for adding task
		ctrl.openPoppupAddTask = () => {
			ctrl.createTaskModal.show();
		};

		// Close poppup for adding task
		ctrl.closeTaskModal = () => {
			ctrl.createTaskModal.hide();
		};
		
		// Open task page
		ctrl.openTask = (id) => {
			$state.go('graph', {id:id});
		};
		

		// Create new task
		ctrl.createTask = (form) => {
			var newTask = {};

		if (form.title.$valid && ctrl.task.hasOwnProperty('title') && ctrl.task.title) {
			if (ctrl.actionStatus === 'addTask') {
				newTask = Object.assign({}, ctrl.task);
				newTask.id = MainService.createId();
				newTask.points = [];
				newTask = MainService.createShirtTitle(newTask, 'long');
				ctrl.tasks.push(newTask);
				ctrl.task = {};
				localStorageService.set(tasksData, ctrl.tasks);
				ctrl.createTaskModal.hide();
			}else if (ctrl.actionStatus === 'editTask') {
				TasksService.editTask(ctrl.task);
				ctrl.tasks = StorageService.getTasks();
				ctrl.createTaskModal.hide();
			}
		}else {
			ctrl.actionTaskError = 'Please write title for task'
		}
		};
	}

})();