(function () {

'use strict';

    angular.module('app').factory('GraphService', function ($log, StorageService, CalendarService) {
        var Service = {

            clearCircles: () => {
                var circles = $('circle');

                circles.each((i, el) => {
                    $(el).css({'fill':''});
                })
            },

            // Edit event in task
            editTaskEvent: function(taskID, ev) {
                var tasks = StorageService.getTasks(),
                    task = tasks[taskID],
                    event = null;

                angular.forEach(task.points, function(point, index) {
                    if (point.id === ev.id) {
                        event = CalendarService.editEvent(ev);
                    }
                });
                StorageService.setTasks(tasks);
            },

            // Delete event from task
            deleteEventFromTask: function(taskID, ev) {
                var tasks = StorageService.getTasks(),
                    task = tasks[taskID];

                angular.forEach(task.points, function(point, index) {
                    if (point.id === ev.id) {
                        CalendarService.deleteEvent(ev);
                        task.points.splice(index, 1);
                    }
                });
                StorageService.setTasks(tasks);
            },
        };

        return Service;
    });

})();