(function() {
  'use strict';

  angular
    .module('app')
    .component('graphComponent', {
      templateUrl: 'js/graph/graph.html',
      controller: graphController
    });

    function graphController(
        $scope, 
        $ionicModal, 
        $log, 
        $element, 
        $attrs, 
        $state, 
        $stateParams,
        $window,
        StorageService,
        localStorageService, 
        CalendarService, 
        MainService, 
        GraphService
        ) {
        var ctrl = this, 
            s = Snap('#complexSVGfromIllustrator'),
            checkedCircle = null,
            path = s.path('')
            .attr({
                fill: 'transparent',
                stroke: '#222',
                strokeWidth: 3
            }),
            pathArray = [],
            activeGraph = null,
            tasks = StorageService.getTasks(),
            currentPoint = {},
            newCirclePoint = null;

        ctrl.events = [];
        ctrl.poppupStatus = 'addEvent';
        ctrl.activeEvent = null;
        // poppups
        ctrl.event = {};
        ctrl.eventFormAction = eventFormAction;
        ctrl.editEventButton = editEventButton;

        // functions
        ctrl.addEventToCalendar = addEventToCalendar;

        //configure the ionic modal before use
        $ionicModal.fromTemplateUrl('event-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then((modal) => {
            ctrl.circleMenu = modal;
        });

        $ionicModal.fromTemplateUrl('remove-point-question.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then((modal) => {
            ctrl.removeCircleQuestion = modal;
        });

        // Set poppup status to editEvent
        function editEventButton() {
            ctrl.poppupStatus = 'editEvent';
        };

        // Start adding or edit event
        function eventFormAction () {
            if (ctrl.poppupStatus === 'addEvent') {
                drawCircle(newCirclePoint);
            }else if (ctrl.poppupStatus === 'editEvent') {
                editEvent(ctrl.activeEvent);    
            }
        };

        // Edint event
        function editEvent(ev) {
            GraphService.editTaskEvent(activeGraph.index, ev);
            tasks = StorageService.getTasks();
            clearGlobalVars();
            updateGraph();
            ctrl.circleMenu.hide();
        };

        // Print current graph
        ctrl.initGraph = () => {
            var result = {},
                tasks = StorageService.getTasks();

            if ( $stateParams.hasOwnProperty('id') &&
                $stateParams.id) {
                result = MainService.findById($stateParams.id, tasks);
                if (result) {
                    activeGraph = result;
                    pathArray = tasks[activeGraph.index].points;
                    printGraph(pathArray);
                }else {
                    $log.error('Graph not found');
                    $state.go('tasks');
                }
            }else {
                $log.error('Graph id not exist');
                $state.go('tasks');
            }
        };

        function addEventToCalendar () {
            if (currentPoint.target.id) {
                MainService.setAppState('point-to-calendar');
                MainService.setActiveEventID(currentPoint.target.id);
                ctrl.circleMenu.hide();
                $state.go('calendar');
            }else {
                $log.error('currentPoint id not found');
                MainService.setAppState('free');
            }
        };

        // Print circles from local storage
        function printGraph(arr) {
            var line = '',
                i = 0,
                event = null;

            for (i; i < arr.length; i++) {
                event = MainService.findEventByID(arr[i].eventID);
                s.circle(arr[i].x, arr[i].y, 15)
                .attr({
                    fill: event.done ? '#80ff00' : '#ff9933',
                    stroke: '#222',
                    strokeWidth: 5,
                    id: arr[i].id
                });

                if (i === 0) {
                    line = 'M ' + arr[i].x + ',' + arr[i].y;
                }else {
                    line += 'L ' + arr[i].x + ',' + arr[i].y;
                    path.attr({ d: line });
                }
            }
        };

        function updatePatch() {
            var first = pathArray[0],
                patchString = 'M ' + first.x + ',' + first.y,
                i;

            for (i in pathArray) {

                if (i !== 0) {
                    patchString += 'L ' + pathArray[i].x + ',' + pathArray[i].y;
                    path.attr({ d: patchString });
                }
            }
            savePoint();

            function savePoint(arr) {
                tasks[activeGraph.index].points = pathArray;
                StorageService.setTasks(tasks);
            }
        };

        // Define event by click on svg
        s.click(function(e) {
            if (e.target.tagName === 'svg') {
                newCirclePoint = e;
                ctrl.poppupStatus = 'addEvent';
                ctrl.event = {};
                ctrl.event.rating = 3;
                ctrl.circleMenu.show();
            }else if (e.target.tagName === 'circle') {
                ctrl.activeEvent = MainService.findEventByID(e.target.id);
                ctrl.event = ctrl.activeEvent;
                currentPoint = e;
                openCircleMenu();
            }
        });

        // Open poppup with circle menu
        function openCircleMenu() {
            ctrl.poppupStatus = 'eventMenu';
            ctrl.circleMenu.show();
        };

        // Close poppup
        ctrl.closePointModal = (status) => {
            if (status === 'removeEventQuestion') {
                ctrl.removeCircleQuestion.hide();
            }else {
                ctrl.circleMenu.hide();
            }
        };

        // Open poppup with question of deleting point
        ctrl.removePointButton = () => {
            ctrl.removeCircleQuestion.show();
        };

        // Open poppup with question of deleting point
        ctrl.removePoint = () => {
            GraphService.deleteEventFromTask(activeGraph.index, ctrl.activeEvent);
            ctrl.circleMenu.hide();
            ctrl.removeCircleQuestion.hide();
            clearGlobalVars();
            updateGraph();
        };

        // Set global values to default
        function clearGlobalVars() {
            ctrl.activeEvent = null;
            ctrl.event = null;
        }

        // Update graph
        function updateGraph() {
            var graph = angular.element(document.querySelector('#complexSVGfromIllustrator')),
                circles = graph.find('circle');

            angular.forEach(circles, function(circle) {
                circle.remove();
            });
            ctrl.initGraph();
        }

        // Define action after choose circle
        function circleEvent(e) {
            checkedCircle = $(e.target);
            GraphService.clearCircles();
            ctrl.activeCircle = $(checkedCircle).attr('id');
            $scope.$apply();
            checkedCircle.css({'fill':'yellow'});
        };

        // Draw new circle
        function drawCircle(e) {
            var date = {},
                event = {},
                result = {},
                circleTag = null,
                events = StorageService.getEvents();

            date.date = {};
            date.date.fullDate = '';
            result = CalendarService.createEvent(ctrl.event, date);
            if (!result.error) {
                event = result.data;
                ctrl.createEventError = null;
            }else {
                ctrl.createEventError = result;
                return false;
            }
            s.circle(e.offsetX, e.offsetY, 15)
            .attr({
                fill: event.done ? '#80ff00' : '#ff9933',
                stroke: '#222',
                strokeWidth: 5,
                id: event.id
            })
            .data('i', pathArray.length);
            pathArray.push({
                x: e.offsetX,
                y: e.offsetY,
                id: event.id,
                eventID: event.id
            });
            updatePatch();
            ctrl.event = {};
            ctrl.circleMenu.hide();
        };
    }

})();