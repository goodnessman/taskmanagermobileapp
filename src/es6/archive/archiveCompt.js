(function() {
  'use strict';

  angular
  .module('app')
  .component('archiveComp', {
	templateUrl: 'js/archive/archive.html',
	controller: archiveController
  });

  function archiveController(
      $rootScope,
      $ionicModal,
      $state,
      CalendarService,
      MainService
  ) {
	var ctrl = this;

	ctrl.archiveEvents = [];
	ctrl.setEventToCalendar = setEventToCalendar;
      ctrl.editEvent = editEvent;
      ctrl.removeEvent = removeEvent;

    ctrl.archiveEvents = CalendarService.getArchiveEvents();

      // Edit event
      function editEvent(ev) {
          MainService.setProperty('activeEvent', ev);
          MainService.setProperty('popupState', 'editEvent');
          MainService.setProperty('appState', 'edit-event');
          $state.go('events');
      }

      // Remove event
      function removeEvent(ev) {
          MainService.setProperty('activeEvent', ev);
          MainService.setProperty('popupState', 'removeEvent');
          MainService.setProperty('appState', 'remove-event');
          $state.go('events');
      }

	// Set Event to calendar
	function setEventToCalendar(ev) {
		MainService.setActiveEvent(ev);
		MainService.setAppState('choose-day');
		$state.go('calendar');
	}
  }

})();
