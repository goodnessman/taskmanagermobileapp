
app.controller('main', function(
    $scope,
    $rootScope,
    $cordovaDevice,
    MainService
    ) {
    var ctrl = this;

    ctrl.editedEvent = {};
    $scope.statData = {};

    // Set 'routerState' and 'routerPrevState' for app
    $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
        MainService.setProperty('routerState', to.name);
        MainService.setProperty('routerPrevState', from.name);
    });

    // Initialize app uuid
    ionic.Platform.ready(function() {
        console.log(ionic.Platform.device());
        if (ionic.Platform.device().uuid) {
            MainService.setProperty('uuid', $cordovaDevice.getDevice().uuid);
            console.log(MainService.getProperty('uuid'));
        }
    });
});
