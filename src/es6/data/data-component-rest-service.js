
(function () {
    'use strict';

    angular.module('app')
        .factory('DataComponentRestService', function ($http, $q) {
            var Service = {

                serverRequest: function (method, url, data) {
                    var deferred = $q.defer(),
                        reqConfig = {};

                    if (method && url) {
                        reqConfig.method = method;
                        reqConfig.url = url;
                        if (data) {
                            reqConfig.data = data;
                        }
                        $http(reqConfig).then(function successCallback(response) {
                            deferred.resolve(response, status);
                        }, function errorCallback(response) {
                            deferred.reject(response, status);
                        });
                    }else {
                        deferred.reject();
                    }

                    return deferred.promise;
                }
            };

            return Service;
        });

})();
