(function() {
  'use strict';

  angular
  .module('app')
  .component('calendarComp', {
     templateUrl: 'js/calendarComp/calendarComp.html',
     controller: calendarCompController,
     bindings: {
         ventdayclick: '=',
         'myData': '=ngModel'
  }
});

function calendarCompController(
    $log,
    $rootScope,
    $scope,
    $ionicModal,
    $timeout,
    config,
    CalendarCompRestService,
    CalendarService,
    MainService,
    StorageService) {
    var ctrl = this;
    var currentEvent = {};
    var date = new Date(),
        currentYearDate = date.getFullYear();

    ctrl.currentMonth = null;
    ctrl.tasks = [];
    ctrl.categories = [];
    ctrl.month = '';
    ctrl.days = null;
    ctrl.activeDay = null;
    // poppup
    ctrl.poppupStatus = 'dayMenu';
    ctrl.createEventError = {};
    ctrl.activeEvent = null;
    ctrl.dayMenuTitle = '';
    ctrl.dayEvents = [];
    // modals
    ctrl.newTaskModal = null;
    // events
    ctrl.events = StorageService.getEvents();
    ctrl.event = {};
    ctrl.event.startTime = date;
    ctrl.eventSources = [];

    // functions
    ctrl.prevMonth = prevMonth;
    ctrl.nextMonth = nextMonth;
    ctrl.dayClick = dayClick;
        // popup
        ctrl.closeTaskModal = closeTaskModal;
        ctrl.createEventButton = createEventButton;
        ctrl.eventFormAction = eventFormAction;
        ctrl.openDayEvent = openDayEvent;
        ctrl.removeEventButton = removeEventButton;
        ctrl.removeEvent = removeEvent;
        ctrl.editEventButton = editEventButton;
        ctrl.editEvent = editEvent;
        ctrl.postponeButton = postponeButton;
        ctrl.addEvToArchive = addEvToArchive;
        ctrl.createSubtaskButton = createSubtaskButton;
        ctrl.configureNewEvent = configureNewEvent;

    // Set 'free' app state as default
    if (MainService.getProperty('appState') !== 'choose-day') {
        MainService.setProperty('appState', 'free');
    }

    //configure the ionic modal before use
    $ionicModal.fromTemplateUrl('new-task-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then((modal) => {
        ctrl.newTaskModal = modal;
    });

    $ionicModal.fromTemplateUrl('remove-event-question.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then((modal) => {
        ctrl.removeEventQuestion = modal;
    });

    // Return current month
    (function(){
        let currentYear = CalendarService.getCurrentYear(currentYearDate);
        console.log(StorageService.getData(config.localStorage.months));

        ctrl.currentMonth = CalendarService.getCurrentMonth(currentYear.date);
        ctrl.month = CalendarService.months[ctrl.currentMonth.date - 1];
        console.log(currentYear);
        console.log(ctrl.currentMonth);
    })();

    // Add event to archive
    function addEvToArchive() {
        CalendarService.setEventToArchive(ctrl.activeEvent);
        ctrl.activeEvent = null;
        ctrl.poppupStatus = 'dayMenu';
        ctrl.event = {};
        $scope.$broadcast('eventChanged');
        ctrl.newTaskModal.hide();
    }

    // Set old events for today
    function setOldEventsForToday() {
        CalendarService.setOldEventsForToday();
        $scope.$broadcast('eventChanged');
    }
    // setOldEventsForToday();

    // Update events if has new feature
    function updateEventFeature() {
        CalendarService.updateEventFeature();
    }
    // updateEventFeature();

    // Add point to calendar
    function addPointToCalendar () {
        var appState = MainService.getAppState(),
            activeEventID = MainService.getActiveEventID(),
            event = MainService.findEventByID(activeEventID);

        if (ctrl.activeDay && event && appState === 'point-to-calendar') {
            event.date = ctrl.activeDay.date;
            CalendarService.editEvent(event);
            ctrl.events = StorageService.getEvents();
            $scope.$broadcast('eventChanged');
            MainService.setAppState('free');
            currentEvent = {};
            ctrl.poppupStatus = 'dayMenu';
        }else {
            $log.error('Error adding point to calendar');
        }
    }

    // Calculate day perfomance
    function calculateDayPerfomance() {

    }

    // Postpone event on new date
    function postponeEvent () {
        var appState = MainService.getAppState(),
            activeEvent = MainService.getActiveEvent();

        if (ctrl.activeDay && activeEvent && appState === 'choose-day') {
            activeEvent.date.day = ctrl.activeDay.date.day;
            activeEvent.date.month = ctrl.activeDay.date.month;
            activeEvent.date.year = ctrl.activeDay.date.year;
            activeEvent.date.fullDate = ctrl.activeDay.date.fullDate;
            if (activeEvent.hasOwnProperty('type') &&  activeEvent.type &&  activeEvent.type === 'archive') {
                activeEvent.date.startDate = ctrl.activeDay.date.fullDate;
            }
            CalendarService.editEvent(activeEvent);
            MainService.setActiveEvent();
            MainService.setProperty('activeDay', {});
            ctrl.activeDay = {};
            MainService.setAppState('free');
            ctrl.poppupStatus = 'dayMenu';
            $scope.$broadcast('eventChanged');
        }
    };

    // Chose date for event
    function postponeButton () {
        MainService.setAppState('choose-day');
        ctrl.activeEvent = {};
        ctrl.event = {};
        ctrl.poppupStatus = 'dayMenu';
        ctrl.newTaskModal.hide();
    };

    // Edit current event
    function editEvent () {
        var dayEvents = null,
            day = MainService.getProperty('activeDay');

        CalendarService.editEvent(ctrl.event);
        day.tasks.push()

        ctrl.events = StorageService.getEvents();
        dayEvents = CalendarService.getDayEvents(ctrl.activeDay, 'dayMenu');
        ctrl.dayEvents = CalendarService.sortDayEvents(dayEvents);
        $scope.$broadcast('eventChanged');
        ctrl.activeEvent = {};
        ctrl.event = {};
        ctrl.poppupStatus = 'dayMenu';
    };

    // Open poppup edit event
    function editEventButton () {
        ctrl.event = MainService.getActiveEvent();
        ctrl.poppupStatus = 'editEvent';
    };

    // Remove event
    function removeEvent (status) {
        var dayEvents = null;

        CalendarService.deleteEvent(ctrl.activeEvent, status);
        dayEvents = CalendarService.getDayEvents(ctrl.activeDay, 'dayMenu');
        ctrl.dayEvents = CalendarService.sortDayEvents(dayEvents);
        $scope.$broadcast('eventChanged');
        ctrl.activeEvent = null;
        ctrl.poppupStatus = 'dayMenu';
        ctrl.event = {};
        ctrl.event.startTime = date;
        ctrl.removeEventQuestion.hide();
    };

    // Open poppup remove event
    function removeEventButton () {
        ctrl.removeEventQuestion.show();
    };

    // Open event from day menu
    function openDayEvent (ev) {
        ctrl.activeEvent = ev;
        MainService.setActiveEvent(ev);
        ctrl.event = ev;
        ctrl.poppupStatus = 'eventShow';
    };

    // start adding or edit event
    function eventFormAction () {
        if (ctrl.poppupStatus === 'addEvent') {
            createEvent();
        }else if (ctrl.poppupStatus === 'addSubtask') {
            createEvent('addSubtask');
        }else if (ctrl.poppupStatus === 'editEvent') {
            ctrl.editEvent(ctrl.activeEvent);
        }else {
            ctrl.editEvent(ctrl.activeEvent);
        }
    };

    // Create new event
    function createEvent(status) {
        var result = {},
            currentEvent,
            day = MainService.getProperty('activeDay');

        if (ctrl.event.title && ctrl.event.title.length > 0) {
            MainService.setProperty('edit-event');
            ctrl.createEventError.error = false;
            ctrl.createEventError.message = '';
            status === 'addSubtask' ? ctrl.event.type = 'subtask' : ctrl.event.type = 'default';
            result = CalendarService.createEvent(ctrl.event, day, null, ctrl.currentMonth);
            if (status === 'addSubtask') {
                ctrl.event.type = 'subtask';
                CalendarService.addSubtask(ctrl.activeEvent, result.data);
            }
            if (!result.error) {
                currentEvent = result.data;
                ctrl.activeEvent = {};
                currentEvent = {};
                ctrl.event = {};
                ctrl.poppupStatus = 'dayMenu';
                ctrl.dayEvents = result.data.tasks;
                MainService.setProperty('day-menu');
                $scope.$broadcast('eventChanged');
            } else {
                ctrl.createEventError.error = true;
                ctrl.createEventError.message = result.message;

                return false;
            }
        } else {
            ctrl.createEventError.error = true;
            ctrl.createEventError.message = 'Please write title for event';
            $log.warn("Title not filled");

            return false;
        }
    };

    // Set default options for new event
    function configureNewEvent() {
        if (ctrl.poppupStatus != 'editEvent') {
            ctrl.event.rating = 3;
            ctrl.event.repeat = {};
            ctrl.event.repeat.repeatDays = {};
            ctrl.event.repeat.repeatDays.mo = true;
            ctrl.event.repeat.repeatDays.tu = true;
            ctrl.event.repeat.repeatDays.we = true;
            ctrl.event.repeat.repeatDays.th = true;
            ctrl.event.repeat.repeatDays.fr = true;
            ctrl.event.repeat.repeatDays.sa = true;
            ctrl.event.repeat.repeatDays.su = true;
            if (ctrl.poppupStatus === 'addEvent' || ctrl.poppupStatus === 'addSubtask') {
                ctrl.event.doneStatus = true;
            }
        }
    }

    // Open popup adding event
    function createEventButton () {
        ctrl.poppupStatus = 'addEvent';
    };

    // Open popup create subtask
    function createSubtaskButton() {
        ctrl.event = {};
        ctrl.event.rating = 3;
        ctrl.poppupStatus = 'addSubtask';
    }

    // Close popup
    function closeTaskModal (status) {
        if (status === 'removeEventQuestion') {
            ctrl.removeEventQuestion.hide()
        }else if (ctrl.poppupStatus === 'dayMenu') {
            ctrl.newTaskModal.hide();
        }
        ctrl.poppupStatus = 'dayMenu';
        ctrl.activeEvent = {};
        ctrl.event = {};
    };

    // Open day menu
    function dayClick(date) {
        var appStatus = MainService.getAppState();

        ctrl.activeDay = date;
        MainService.setProperty('activeDay', date);
        if (appStatus === 'free') {
            openDayMenu(date);
            return true;
        }else if (appStatus === 'choose-day') {
            postponeEvent();
        }else if (appStatus === 'point-to-calendar') {
            addPointToCalendar();
        }
    };

    // Open day menu
    function openDayMenu () {
        var dayEvents = MainService.getProperty('activeDay').tasks;

        ctrl.createEventError.error = false;
        ctrl.dayEvents = CalendarService.sortDayEvents(dayEvents);
        ctrl.dayMenuTitle = moment(ctrl.activeDay.date.fullDate).format('DD') + '-' +
        moment(ctrl.activeDay.date.fullDate).format('MM') + '-' +
        moment(ctrl.activeDay.date.fullDate).format('YYYY') + ' ' +
        moment(ctrl.activeDay.date.fullDate).format('dddd');
        ctrl.newTaskModal.show();
    };

    function prevMonth() {
        CalendarService.prev();
        ctrl.month = CalendarService.getMonth();
        ctrl.days = CalendarService.getDays();
    }

    function nextMonth() {
        ctrl.currentMonth = CalendarService.next(ctrl.currentMonth);
        console.log(ctrl.currentMonth);
        ctrl.month = CalendarService.months[ctrl.currentMonth.date - 1];
    }
}

})();
