(function() {
  'use strict';

  angular
    .module('app')
    .component('dayComp', {
      templateUrl: 'js/calendarComp/dayComp/dayComp.html',
      controller: dayCompController,
      bindings: {
          day: '<',
          month: '<',
          events: '='
        }
    });

    function dayCompController($scope, CalendarService) {
      var ctrl = this;

      ctrl.today = CalendarService.checkToday(ctrl.day.date.day, ctrl.month);
    }

})();