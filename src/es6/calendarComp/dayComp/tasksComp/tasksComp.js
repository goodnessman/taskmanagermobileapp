(function() {
  'use strict';

  angular
    .module('app')
    .component('tasksComp', {
      templateUrl: 'js/calendarComp/dayComp/tasksComp/tasksComp.html',
      controller: tasksCompController,
      bindings: {
          day: '='
        }
    });

    function tasksCompController($scope, CalendarService, StorageService) {
      var ctrl = this;

      $scope.$on('eventChanged', function () {
        // console.log('test');
      });
    }

})();
