(function() {
  'use strict';

  angular
  .module('app')
  .component('ratingComp', {
     templateUrl: 'js/calendarComp/ratingComp/ratingComp.html',
     controller: ratingCompController,
     bindings: {
      event: '='
  }
});

function ratingCompController(
    $scope,
    $ionicModal,
    $timeout,
    CalendarCompRestService,
    CalendarService,
    MainService,
    StorageService) {
    var ctrl = this;
    var currentEvent = {};
    var date = new Date();

    ctrl.tasks = [];
    ctrl.checkRating = checkRating;

    // Select rating for event
    function checkRating(rate) {
        console.log(ctrl.event);
    }

    // Update events if has new feature
    function updateEventFeature() {
        CalendarService.updateEventFeature();
    }
}

})();