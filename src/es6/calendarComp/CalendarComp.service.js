(function () {
'use strict';

angular.module('app')
.factory('CalendarService', function ($log, config, StorageService, MainService) {
    var d = new Date(),
        currentYear = d.getFullYear(),
        currentMonth = d.getUTCMonth(),
        today = d.getDate(),
        firstDay = new Date(currentYear, currentMonth, 1),
        firstWday = firstDay.getDay(),
        oneHour = 1000 * 60 * 60,
        oneDay = oneHour * 24,
        nextMonth = new Date(currentYear, currentMonth + 1, 1),
        lastDate = Math.ceil((nextMonth.getTime() - firstDay.getTime() - oneHour) / oneDay),
        trigger = d.getUTCMonth();

    var Service = {
        months: ["Январь","Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],

        // Set event to archive
        setEventToArchive: function(ev) {
            var events = StorageService.getEvents();

            angular.forEach(events, function(event, index) {
                if (event.id === ev.id) {
                    event.date.fullDate = '';
                    event.date.startDate = '';
                    event.type = 'archive';
                }
            });
            StorageService.setEvents(events);
        },

        // Get archive events
        getArchiveEvents: function() {
            var events = StorageService.getEvents(),
                result = [];

            angular.forEach(events, function(event, index) {
                if (event.hasOwnProperty('type') &&
                    event.type &&
                    event.type === 'archive') {
                    result.push(event);
                }
            });
            return result;
        },

        // Get today day
        getTodayday: function() {
            var today = moment(new Date),
                date = {};

            date.day = parseInt(today.format('DD'));
            date.month = parseInt(today.format('MM'));
            date.year = parseInt(today.format('YYYY'));
            date.fullDate = today.format('YYYY-MM-DD');
            return date;
        },

        // Set ald events for today
        setOldEventsForToday: function() {
            var events = StorageService.getEvents(),
                today = moment(new Date),
                eventStart = null,
                diffDays = 0,
                startDate,
                todayData = Service.getTodayday();

            angular.forEach(events, function(event, index) {
                eventStart = moment(event.date.startDate);
                diffDays = eventStart.diff(today, 'days');
                if (diffDays < -1 && event.done === false && !(moment(event.date.fullDate).isAfter(today))) {
                    startDate = event.date.startDate;
                    event.date = todayData;
                    event.date.startDate = startDate;
                }
            });
            StorageService.setEvents(events);
        },

        // Sort day event by priority
        sortDayEvents: function(events) {
            var doneEvents = [],
                inProgressEvents = [],
                sumEvents = [];

            angular.forEach(events, function(event, index) {
                if (event.done) {
                    doneEvents.push(event);
                }else {
                    inProgressEvents.push(event);
                }
            });
            doneEvents.sort(sortNumber);
            inProgressEvents.sort(sortNumber);
            inProgressEvents.reverse();
            doneEvents.reverse();
            sumEvents = inProgressEvents.concat(doneEvents);
            return sumEvents;

            function sortNumber(a,b) {
                return a.rating - b.rating;
            }
        },

        // Get day count from event was started
        getEventLatencyStatus: function(events) {
            var today = null,
                eventStart = null,
                diffDays = 0,
                status = null;

            angular.forEach(events, function(ev) {
                today = moment(ev.date.fullDate);
                eventStart = moment(ev.date.startDate);
                diffDays = today.diff(eventStart, 'days');
                if (diffDays >= 7) {
                    ev.latencyStatus = 7;
                }else if (diffDays >= 6) {
                    ev.latencyStatus = 6;
                }else if (diffDays >= 5) {
                    ev.latencyStatus = 5;
                }else if (diffDays >= 4) {
                    ev.latencyStatus = 4;
                }else if (diffDays >= 3) {
                    ev.latencyStatus = 3;
                }else if (diffDays >= 2) {
                    ev.latencyStatus = 2;
                }else {
                    ev.latencyStatus = 1;
                }
            });
            return events;
        },

        // Update events if has new feature
        updateEventFeature: function() {
            var events = StorageService.getEvents();

            angular.forEach(events, function(event, index) {
                if (event.done) {
                    event.progress = 100;
                }
            });
            StorageService.setEvents(events);
        },

        // Delete repeated events
        deleteRepeatedEvents: function (ev) {
            var events = StorageService.getEvents(),
                i = 0,
                len = 0,
                result = null,
                newArr = [];

            for (i, len = events.length; i < len; i++) {
                if (events[i].repeat.repeat && events[i].repeat.repeatID === ev.repeat.repeatID) {
                    result = events[i];
                    continue;
                }else {
                    newArr.push(events[i]);
                }
            }
            StorageService.setEvents(newArr);
            return result;
        },

        // Delete event
        deleteEvent: function (ev, status) {
            var events = StorageService.getEvents(),
                i = 0,
                len = 0,
                result = null;

            for (i, len = events.length; i < len; i++) {
                if (ev.id === events[i].id &&
                        status &&
                        status === 'repeatedEvents' &&
                        events[i].repeat.repeat) {
                    result = Service.deleteRepeatedEvents(events[i]);
                    break;
                }else if (ev.id === events[i].id && !status) {
                    result = events[i];
                    if (events[i].type === 'subtask') {
                        events = Service.calculateParentEvent(events[i].parentEventID, events, 'deleteEvent', events[i]);
                    }
                    events.splice(i, 1);
                    break;
                }
            }
            if (!status) {
                StorageService.setEvents(events);
            }
            return result;
        },

        // Create shirt title for event
        createEventTitles: function (ev) {
            if (ev.title.length > 3) {
                ev.shirtTitle = ev.title[0] + ev.title[1] + ev.title[2] + '..';
            }
            if (ev.date.fullDate) {
                ev.titleDateFormat = moment(ev.date.fullDate).format('DD') + '-' +
                moment(ev.date.fullDate).format('MM') + '-' +
                moment(ev.date.fullDate).format('YYYY') + ' ' +
                moment(ev.date.fullDate).format('dddd');
            }
            return ev;
        },

        // Return event
        getEvent: function (ev) {
            var events = StorageService.getEvents(),
                result = false;

            angular.forEach(events, function(event, index) {
                if (ev.id === event.id) {
                    result = true;
                }
            });

            if (result) {
                return ev;
            }else {
                return null;
            }
        },

        // Check simple events
        checkSimpleEvent: function (ev) {
            var events = StorageService.getEvents(),
                result = false;

            angular.forEach(events, function(event, index) {
                if (ev.title === event.title) {
                    result = true;
                }
            });

            if (result) {
                return ev;
            }else {
                return null;
            }
        },

        // Return arr day for repeating
        getRepeatDays: function(ev) {
            var result = [];
            if (ev.hasOwnProperty('repeat') && ev.repeat.hasOwnProperty('repeatDays')) {
                if (ev.repeat.repeatDays.hasOwnProperty('mo') && ev.repeat.repeatDays.mo) {
                    result.push('mo');
                }
                if (ev.repeat.repeatDays.hasOwnProperty('tu') && ev.repeat.repeatDays.tu) {
                    result.push('tu');
                }
                if (ev.repeat.repeatDays.hasOwnProperty('we') && ev.repeat.repeatDays.we) {
                    result.push('we');
                }
                if (ev.repeat.repeatDays.hasOwnProperty('th') && ev.repeat.repeatDays.th) {
                    result.push('th');
                }
                if (ev.repeat.repeatDays.hasOwnProperty('fr') && ev.repeat.repeatDays.fr) {
                    result.push('fr');
                }
                if (ev.repeat.repeatDays.hasOwnProperty('sa') && ev.repeat.repeatDays.sa) {
                    result.push('sa');
                }
                if (ev.repeat.repeatDays.hasOwnProperty('su') && ev.repeat.repeatDays.su) {
                    result.push('su');
                }
            }
            return result;
        },

        // Create repeted events
        createReapeteEvents: function(ev) {
            var events = [],
                newEvents = [],
                daysOfWeek = {Mo:'mo',Tu:'tu',We:'we',Th:'th',Fr:'fr',Sa:'sa',Su:'su'},
                newEv = {},
                newDate = null,
                day = 0,
                i = 7;

            while (i--) {
                newDate = moment(ev.date.fullDate).add(day, 'day').format('YYYY-MM-DD');
                day++;
                newEv = angular.copy(ev);
                if (newEv.repeat.days.indexOf(daysOfWeek[moment(newDate).format('dd')]) !== -1) {
                    newEv.id = MainService.createId();
                    newEv.date.fullDate = newDate;
                    newEv.date.startDate = newDate;
                    newEvents.push(newEv);
                }
            }
            if (newEvents.length > 0) {
                events = StorageService.getEvents();
                events = events.concat(newEvents);
                StorageService.setEvents(events);
            }
        },

        addSubtask: function(ev, subtask) {
            var events = StorageService.getEvents();

            events.some(function(event) {
                if (ev.id === event.id) {
                    event.subtasks.push(subtask.id);
                    events = Service.calculateParentEvent(event.id, events);
                    return true;
                }
            });
            StorageService.setEvents(events);
        },

        // Return default empty event
        getEmptyEvent() {
            let event = null;

            event = {
                id: MainService.createId(),
                title: '',
                shirtTitle: '',
                titleDateFormat: '',
                type: 'default',
                subtasks: [],
                done: false,
                progress: 0,
                doneStatus: true,
                rating: 3,
                date: {
                    startDate: null,
                    fullDate: null
                },
                repeat: {}
            };

            return event;
        },

        // Save event
        saveEvent: (ev) => {
            let day = MainService
            console.log();
        },

        // Get month by date
        getMonthByDate: (year, month) => {
            let months = StorageService.getData(config.localStorage.months),
                result = null;

            months.some(function(item) {
                if ( item.year == year && item.date == month) {
                    result = item;
                    return true;
                }
            });

            return result;
        },


        // Create new event
        createEvent: function (ev, day, activeEvent, month) {
            var event = Service.getEmptyEvent(),
                result = {},
                events = [],
                repeatDays = null;

            activeEvent = activeEvent ? activeEvent : MainService.getProperty('activeEvent');
            event.title = ev.title;
            event.type = ev.type;
            ev.type === 'subtask' ? event.parentEventID = activeEvent.id : event.parentEventID = null;
            event.rating = ev.rating;
            event.date = day.date;
            event.date.startDate = day.date.fullDate;

            repeatDays = Service.getRepeatDays(ev);
            event = Service.createEventTitles(event);
            if (repeatDays.length > 0 && ev.repeat.repeat) {
                event.repeat.repeat = true;
                event.repeat.days = repeatDays;
                event.repeat.repeatDays = ev.repeat.repeatDays;
                event.repeat.repeatID = MainService.createId();
                Service.createReapeteEvents(event);
            }else {
                event.repeat.repeat = false;
                event.repeat.days = [];
                event.repeat.repeatDays = {};
                day.tasks.push(event);
                if (day.tasks.length > 1) {
                    day.tasks = Service.sortDayEvents(day.tasks);
                    day.tasks = Service.getEventLatencyStatus(day.tasks);
                }
                Service.saveDay(day, month);
            }
            result.data = day;
            result.error = false;
            result.message = '';

            return result;
        },

        // Save Day
        saveDay: (day, month) => {
            var saveMonth = false;

            month.days.some((dayItem, index) => {
                if (dayItem.id === day.id) {
                    month.days[index] = angular.copy(day);
                    saveMonth = true;

                    return true;
                }
            });
            if (saveMonth) {
                Service.saveMonth(month);
            }

            return saveMonth;
        },

        // save Month
        saveMonth: (month) => {
            let months = StorageService.getData(config.localStorage.months),
                saveMonth = false;

            months.some((monthItem, index) => {
                if (monthItem.id === month.id) {
                    months[index] = angular.copy(month);
                    saveMonth = true;

                    return true;
                }
            });
            StorageService.setData(config.localStorage.months, months);

            return saveMonth;
        },

        // Save task
        saveTask: (task) => {
            let months = StorageService.getData(config.localStorage.months);

            months.some((month) => {
                if (month.year == moment(task.date.fullDate).format('YYYY'), month.date == moment(task.date.fullDate).format('M')) {
                    month.days.some((item) => {
                        if (moment(item.date.fullDate).format('D') == moment(task.date.fullDate).format('D')) {
                            item.tasks.push(task);

                            return true;
                        }
                    });

                    return true;
                }
            });

            StorageService.setData(config.localStorage.months, months);
        },

        // get Day By Month
        getDayByMonth: (month, day) => {
            let result = null;

            month.days.some((item) => {
                if (moment(item.date.fullDate).format('D') == day) {
                    result = item;

                    return true;
                }
            });

            return result;
        },

        // Edit repeated events
        editRepeatedEvents: function (evOld, evNew) {
            var events = StorageService.getEvents(),
                i = 0,
                len = 0,
                result = null;

            for (i, len = events.length; i < len; i++) {
                if (events[i].repeat.repeat && events[i].repeat.repeatID === evOld.repeat.repeatID) {
                    events[i] = Service.editCurrentEvent(events[i], evNew, 'repeatedEvents');
                    result = events[i];
                }
            }
            StorageService.setEvents(events);
            return result;
        },

        // Edit event
        editEvent: function (task) {
            let months = StorageService.getData(config.localStorage.months),
                result = null;

            months.some((month) => {
                if (month.year == moment(task.date.fullDate).format('YYYY'), month.date == moment(task.date.fullDate).format('M')) {
                    month.days.some((item) => {
                        if (moment(item.date.fullDate).format('D') == moment(task.date.fullDate).format('D')) {
                            month.days.tasks.some((taskItem) => {
                                if (taskItem.id === task.id) {
                                    if (task.id === taskItem.id && task.editAllRepeatedEvents && taskItem.repeat.repeat) {
                                        result = Service.editRepeatedEvents(taskItem, task);
                                        return true;
                                    } else if (ev.id === taskItem.id) {
                                        taskItem = Service.editCurrentEvent(taskItem, task);
                                        if (taskItem.type === 'subtask' && taskItem.progressChanged) {
                                            events = Service.calculateParentEvent(taskItem.parentEventID, events);
                                        }
                                        result = taskItem;
                                        return true;
                                    }
                                    return true;
                                }
                            });
                            return true;
                        }
                    });
                    return true;
                }
            });

            StorageService.setData(config.localStorage.months, months);


            var events = StorageService.getEvents(),
                i = 0,
                len = 0,
                result = null;

            for (i, len = events.length; i < len; i++) {
                if (ev.id === events[i].id && ev.editAllRepeatedEvents && events[i].repeat.repeat) {
                    result = Service.editRepeatedEvents(events[i], ev);
                    break;
                }else if (ev.id === events[i].id) {
                    events[i] = Service.editCurrentEvent(events[i], ev);
                    if (events[i].type === 'subtask' && events[i].progressChanged) {
                        events = Service.calculateParentEvent(events[i].parentEventID, events);
                    }
                    result = events[i];
                    break;
                }
            }

            if (!ev.editAllRepeatedEvents) {
                StorageService.setEvents(events);
            }
            return result;
        },

        // Recalculate progress parent event when children event change
        calculateParentEvent: function(parentID, events, status, currentChild) {
            var childEvs = [],
                avg = 0,
                sum = 0;

            events.some(function(event) {
                if (status && status === 'editEvent') {
                    event.subtasks.some(function(el, idex) {
                        if (el === currentChild.id) {
                            event.subtasks.splice(idex, 1);
                        }
                        childEvs.push((MainService.findById(el, events)).el);
                    });
                }
                if (event.id === parentID) {
                    event.subtasks.map(function(el) {
                        childEvs.push((MainService.findById(el, events)).el);
                    });
                    if (childEvs.length > 1) {
                        childEvs.map(function(el) {
                            if (el && el.progress) {
                                sum += parseInt(el.progress, 10);
                            }
                        });
                        avg = Math.floor(sum/event.subtasks.length);
                        event.progress = avg;
                        if (event.progress === 100) {
                            event.done = true;
                        }
                    }
                    return true;
                }
            });
            return events;
        },

        // Edit event
        editCurrentEvent: function(obj1, obj2, status) {
            var type = MainService.getAppState();

            obj1.title = obj2.title;
            if (!status || status !== 'repeatedEvents') {
                (obj1.type === 'subtask' && (obj2.done !== obj1.done || obj2.progress !== obj1.progress)) ? obj1.progressChanged = true : obj1.progressChanged = false;
                obj1.progress = obj2.progress;
                obj1.done = obj2.done;
                if (obj2.doneStatus) {
                    obj2.done ? obj1.progress = 100 : obj1.progress = 0;
                }
                obj1.date.day = obj2.date.day;
                obj1.date.month = obj2.date.month;
                obj1.date.year = obj2.date.year;
                obj1.date.fullDate = obj2.date.fullDate;
                obj1.day = obj2.date.day;
            }
            obj1.rating = obj2.rating;
            obj1.doneStatus = obj2.doneStatus;
            if (type === 'choose-day') {
                obj1.type = 'calendar';
                obj1.date.startDate = obj2.date.startDate;
            }
            return obj1;
        },

        // Return events for day
        getDayEvents: function (dayDate, type) {
            var events = StorageService.getEvents(),
                result = [];

            if (events.length > 0) {
                angular.forEach(events, function(event) {
                    if (event &&
                        typeof dayDate !== 'string' &&
                        dayDate.date.day !== ' ' &&
                        event.date.fullDate === dayDate.date.fullDate &&
                        event.type != 'subtask') {
                        if (event.subtasks.length > 0 && type === 'dayMenu') {
                            event.subtasksData = Service.getSubtasks(event);
                        }
                        result.push(event);
                    }
                });
            }
            return result;
        },

        getSubtasks: function(ev) {
            var events = StorageService.getEvents(),
                result = [];

            ev.subtasks.map(function(el) {
                angular.forEach(events, function(event) {
                    if (el === event.id) {
                        result.push(event);
                    }
                });
            });

            return result;
        },

        // Check current day
        checkToday: function(day, month) {
            var date = new Date(),
                today = date.getDate();

            if (day == today && month.date == date.getUTCMonth() + 1) {
                return true;
            }
            return false;
        },

        // Return empty instance year
        getEmptyYear: function(yearNumber) {
            var i = 1,
                year = null,
                years = null,
                d = new Date(),
                currentYear = d.getFullYear();

            year = {
                id: MainService.createId(),
                type: 'year',
                date: yearNumber || currentYear,
                months: []
            };

            while (i < 13) {
              year.months.push(Service.getEmptyMonth(i, year.date).id);
              i++;
            }

            return year;
        },

        // Return empty instance month
        getEmptyMonth: function(monthNumber, year) {
            var month = null,
                d = new Date(),
                currentMonth = d.getUTCMonth(),
                months = null;

            month = {
                id: MainService.createId(),
                type: 'month',
                year: year,
                date: monthNumber || ++currentMonth,
                days: null
            };
            month.days = Service.getDays(month.date, year);

            months = StorageService.getData(config.localStorage.months);

            if (months) {
                months.push(month);
            }else {
                months = [];
                months.push(month);
            }

            StorageService.setData(config.localStorage.months, months);

            return month;
        },

        // Return empty day
        getEmptyDay: function(currDay, currMonth, currYear) {
            var day = null;

            day = {
                id: MainService.createId(),
                type: 'day',
                status: (currYear && currMonth && currDay) ? 'default' : 'empty',
                date: {
                    day: currDay || null,
                    month: currMonth || null,
                    year: currYear || null,
                    fullDate:  null,
                    dayOfWeek: null
                },
                tasks: []
            };
            day.date.fullDate = (currYear && currMonth && currDay) ? moment(day.date.year + '-' + day.date.month + '-' + day.date.day).format("YYYY-MM-DD") : null;
            day.date.dayOfWeek = day.date.fullDate ? moment(day.date.fullDate).format('dd') : null;

            return day;
        },

        // Return object current month
        getCurrentMonth: function(year) {
            var d = new Date(),
                currentMonth = d.getUTCMonth(),
                result = null,
                months = null;

            months = StorageService.getData(config.localStorage.months);

            months.some(function(month) {
                if (month.date === (currentMonth + 1) && year === month.year) {
                    result = month;
                    return true;
                }
            });

            return result;
        },

        // Get current year
        getCurrentYear: function(yearDate) {
            var data = StorageService.getData(config.localStorage.years),
                result = null;

            if (data.length) {
                data.some(function(year) {
                    if (year.date === yearDate) {
                        result = year;
                        return true;
                    }
                });
                if (!result) {
                    result = Service.getEmptyYear();
                    data.push(result);
                    StorageService.setData(config.localStorage.years, data);
                }
            }else {
                let arr = [];

                result = Service.getEmptyYear();
                arr.push(result);
                StorageService.setData(config.localStorage.years, arr);
            }

            return result
        },

        // Get current month
        getMonth: function() {
            return months[currentMonth];
        },

        // Formatting days for print
        getDays: function(month, year) {
            var firstDay = new Date(year, month, 1),
                firstWday = firstDay.getDay(),
                nextMonth = new Date(year, month + 1, 1),
                lastDate = Math.ceil((nextMonth.getTime() - firstDay.getTime() - oneHour) / oneDay),
                days = [],
                i = 0, j = 0;

            firstWday = firstDay.getDay();
            (firstWday != 0) ? firstWday = firstWday - 1 : firstWday = 0;
            for (i; i < lastDate; i++) {
                if ((1 + j) > 31) {
                    break;
                }
                if (i == firstWday) {
                    for (j; j < lastDate; j++) {
                        days.push(Service.getEmptyDay(1 + j, month, year));
                    }
                }else {
                    if (j < 10) {
                        days.push(Service.getEmptyDay());
                    }else {
                        continue;
                    }
                }
            }

            return days;
        },

        getMonth2: function(date) {
            console.log('test');
        },

        // Next month
        next: function(month) {
            var year = null,
                result = null;

            if (month.date === 12) {
                year = Service.getCurrentYear(month.year + 1);
                result = year.months[month.date];
            } else {
                year = Service.getCurrentYear(month.year);
                result = year.months[month.date];
            }

            return result;
        },

        // Previos month
        prev: function() {
            if (--trigger === -1) {
                trigger = 11;
            }
            month = trigger;
            today = d.getDate();
            firstDay = new Date(currentYear, month, 1);
            firstWday = firstDay.getDay();
            nextMonth = new Date(currentYear, month + 1, 1);
            lastDate = Math.ceil((nextMonth.getTime() - firstDay.getTime() - oneHour)/oneDay);
        }
    };

    return Service;
});

})();
