(() => {

'use strict';

    angular.module('app').factory('StorageService', ($log, $q, config, localStorageService) => {
        var Service = {

            // Set events
            setEvents: (arr) => {
                localStorageService.set(config.localStorage.events, arr);
            },

            // Get events
            getEvents: () => {
                var result = localStorageService.get(config.localStorage.events);

                if (!result) {
                    result = [];
                }

                return result;
            },

            // Set data
            setData: (key, data) => {
                localStorageService.set(key, data);
            },

            // Get data by parameter
            getData: (data) => {
                var result = localStorageService.get(data);

                if (!result) {
                    result = [];
                }

                return result;
            },

            // Set tasks
            setTasks: (arr) => {
                localStorageService.set(config.localStorage.tasks, arr);
            },

            // Get tasks
            getTasks: () => {
                var result = localStorageService.get(config.localStorage.tasks);
                if (result) {
                    return result;
                } else {
                    return [];
                }
            },

            // Get task by id
            getTask: (id) => {
                var tasks = [],
                    i = 0,
                    result = [];

                if (localStorageService.get(config.localStorage.tasks)) {
                    tasks = localStorageService.get(config.localStorage.tasks);
                    for (i = tasks.length - 1; i >= 0; i--) {
                        if (tasks[i].hasOwnProperty('id') &&
                            tasks[i].id === id) {
                            result.push(tasks[i]);
                        }
                    }
                    if (!(result.length)) {
                        $log.error('Tasks not found');
                        return false;
                    }else if (result.length > 1) {
                        $log.error('Found 2 similar task');
                        return false;
                    }else {
                        return result[0];
                    }
                } else {
                    $log.error('Tasks array not exist');
                }
            }

        };

        return Service;
    });

})();
