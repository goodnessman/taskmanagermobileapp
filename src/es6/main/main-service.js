(function() {

'use strict';

    angular.module('app').factory('MainService', function($rootScope, $log, config, StorageService) {
        var Service = {
            appState: 'free',
            activeEventID: null,
            activeEvent: null,
            activeDay: null,
            popupState: 'dayMenu',
            routerState: null,
            routerPrevState: null,
            uuid: 'admin',

            // Return service private properties
            getProperty: function(prop) {
                if (prop && typeof(prop) === 'string' && Service[prop] !== 'undefined') {
                    return Service[prop];
                }
                return null;
            },

            // Set service private properties
            setProperty: function(prop, val) {
                if (prop && typeof(prop) === 'string' && Service.hasOwnProperty(prop)) {
                    if (typeof val === 'object') {
                        Service[prop] = angular.copy(val);
                    } else {
                        Service[prop] = val;
                    }

                    return Service[prop];
                }
                return null;
            },

            // Edit event
            editEvent: function(ev) {
                $rootScope.$broadcast('editEvent', {
                    data: ev
                });
            },
            // Set active event
            setActiveEvent: (ev) => {
                ev ? Service.activeEvent = ev : Service.activeEvent = null;
            },

            // Get active event
            getActiveEvent: () => {
                return Service.activeEvent;
            },

            // Set active event
            setActiveEventID: (id) => {
                Service.activeEventID = id;
            },

            // Get active event
            getActiveEventID: () => {
                return Service.activeEventID;
            },

            // Return app state
            getAppState: () => {
                return Service.appState;
            },

            // Set new app state
            setAppState: (state) => {
                if (state) {
                    Service.appState = state;
                }else {
                    $log.error("Wrong state");
                }
            },

            // Create id for event by current time
            createId: () => {
                var date = new Date(),
                    day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear(),
                    hours = date.getHours(),
                    minutes = date.getMinutes(),
                    seconds = date.getSeconds(),
                    milliseconds = date.getMilliseconds(),
                    id = '',
                    random = ('-' + Math.random()).slice(2);

                id = year + '-' + month + '-' + day + '-' + hours + '-' + minutes + '-' + seconds + '-' + milliseconds + '-' + random;
                return id;
            },

            // Create shirt title
            createShirtTitle: function(obj, titleFormat) {
                if (obj.hasOwnProperty('title') && obj.title.length > 3) {
                    if (titleFormat) {
                        if (titleFormat === 'shirt') {
                            obj.shirtTitle = restrictString(obj.title, 3);

                        }else if (titleFormat === 'long') {
                            if (obj.title.length > 16) {
                                obj.shirtTitle = restrictString(obj.title, 15);
                            }else {
                                return obj;
                            }
                        }else {
                            obj.shirtTitle = restrictString(obj.title, 3);
                        }
                    }else {
                        obj.shirtTitle = restrictString(obj.title, 3);
                    }
                }
                return obj;

                function restrictString(string, number) {
                    string = string.substr(0, number);
                    string = string + '...';
                    return string;
                }
            },

            // Find event by id
            findEventByID: function (id) {
                var events = StorageService.getEvents(),
                    result = Service.findById(id, events);

                    return result.el;
            },

            // Search event by id
            findById: (id, arr) => {
                var result = {};

                arr.some(function(el, index) {
                    if (el.id === id) {
                        result.el = el;
                        result.index = index;
                        return true;
                    }
                })
                return result;
            }
        };

        return Service;
    });

})();
